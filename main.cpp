#include "ex01/linear_combination.hpp"
#include "ex02/lerp.hpp"
#include "ex03/dot.hpp"
#include "ex04/norm.hpp"
#include "ex05/angle_cos.hpp"
#include "ex06/cross_product.hpp"
#include "ex08/trace.hpp"
#include "ex09/transpose.hpp"
#include "ex10/row_echelon.hpp"
#include "ex11/determinant.hpp"
#include "ex12/inverse.hpp"
#include "ex12/substitutions.hpp"
#include "ex13/rank.hpp"
#include "inc/matrix.hpp"
#include "inc/vector.hpp"
#include <algorithm>
#include <complex>
#include <functional>
#include <iostream>
#include <stdexcept>

using std::cout;
using std::endl;
using std::string;

auto oks = 0;
auto kos = 0;

const auto color_red    = std::string("\033[31m");
const auto color_green  = std::string("\033[32m");
const auto color_cyan   = std::string("\033[36m");
const auto color_yellow = std::string("\033[33m");
const auto color_normal = std::string("\033[0;39m");

// returns string according to condition. Counts number of success/failures
string show_ok(bool b)
{
	if (b) {
		oks++;
		return color_green + "ok" + color_normal;
	}
	kos++;
	return color_red + "KO" + color_normal;
}

// prints success/failure ratio
void print_results()
{
	// handling plural forms
	const auto tests  = (oks > 1 ? "tests" : "test");
	const auto all    = (kos == 0 ? "All " : "");
	const auto issues = (kos > 1 ? "issues" : "issue");
	const auto were   = (kos > 1 ? "were" : "was");

	cout << '\n' + color_cyan;
	if (kos != 0) {
		cout << "Please check the tests and fix the " << issues << ".\n";
		cout << "(" << color_red << kos << color_cyan << " " << issues << " "
		     << were << " found)\n";
	}
	cout << all << color_green << oks << color_cyan << " " << tests
	     << " passed.\n";
	cout << color_normal;
}

// to compare for bool equality
bool is_close(double a, double b)
{
	const auto limit = 0.0001;
	const auto diff  = std::abs(a - b);
	return diff < limit;
}
// same but for complex numbers
template <typename T>
bool is_close(std::complex<T> a, std::complex<T> b)
{
	return is_close(a.real(), b.real()) && is_close(a.imag(), b.imag());
}
// sanme but for vectors
template <typename K, std::size_t n>
bool is_close(const Vector<K, n>& a, const Vector<K, n>& b)
{
	for (decltype(n) i = 0; i < n; i++) {
		if (!is_close(a[i], b[i])) {
			return false;
		}
	}
	return true;
}
// sanme but for matrices
template <typename K, std::size_t m, std::size_t n>
bool is_close(const Matrix<K, m, n>& a, const Matrix<K, m, n>& b)
{
	for (decltype(m) i = 0; i < m; i++) {
		if (!is_close(a[i], b[i])) {
			return false;
		}
	}
	return true;
}

// exercise 00 (scale with *, add/sub with + and -)
void test_scale_add_sub()
{
	cout << color_yellow << "=== test_scale_add_sub ===\n" << color_normal;

	// matrices
	auto m1 = Matrix<int, 2, 3>({
	    {3, 6, 8},
	    {4, 7, 9},
	});
	auto m2 = Matrix<int, 2, 3>({
	    {5, 6, 8},
	    {4, 7, 9},
	});
	auto mr = Matrix<int, 2, 3>({
	    {3 + 5, 6 + 6, 8 + 8},
	    {4 + 4, 7 + 7, 9 + 9},
	});
	cout << "m1 + m2"
	     << " -> " << show_ok(mr == m1 + m2) << endl;
	m1 = Matrix<int, 2, 3>({
	    {3,   6, 8},
	    {4, 128, 9},
	});
	m2 = Matrix<int, 2, 3>({
	    {5,  6, 8},
	    {4, -7, 9},
	});
	mr = Matrix<int, 2, 3>({
	    {3 * 2 + 5 * 3,    6 * 2 + 6 * 3, 8 * 2 + 8 * 3},
	    {4 * 2 + 4 * 3, 128 * 2 + -7 * 3, 9 * 2 + 9 * 3},
	});
	cout << "2 * m1 + m2 * 3"
	     << " -> " << show_ok(mr == 2 * m1 + m2 * 3) << endl;
	mr = Matrix<int, 2, 3>({
	    {3 * 2 - 5 * 3,    6 * 2 - 6 * 3, 8 * 2 - 8 * 3},
	    {4 * 2 - 4 * 3, 128 * 2 - -7 * 3, 9 * 2 - 9 * 3},
	});
	cout << "-2 * m1 - m2 * -3"
	     << " -> " << show_ok(-1 * mr == -2 * m1 - m2 * -3) << endl;

	// vectors
	auto v1 = Vector<int, 4>({-3, 60, 5, 0});
	auto v2 = Vector<int, 4>({3, 6, -8, 1});
	auto vr = Vector<int, 4>({
	    -2 * -3 + 3 * -3,
	    -2 * 60 + 6 * -3,
	    -2 * 5 + -8 * -3,
	    -2 * 0 + 1 * -3,
	});
	cout << "-2 * v1 + v2 * -3"
	     << " -> " << show_ok(vr == -2 * v1 + v2 * -3) << endl;
	vr = Vector<int, 4>({
	    -2 * -3 - 3 * -3,
	    -2 * 60 - 6 * -3,
	    -2 * 5 - -8 * -3,
	    -2 * 0 - 1 * -3,
	});
	cout << "-2 * v1 - v2 * -3"
	     << " -> " << show_ok(vr == -2 * v1 - v2 * -3) << endl;
}

// exercise 01 (sum of all vectors multiplied by their coefficients)
void test_linear_combination()
{
	cout << color_yellow << "=== test_linear_combination ===\n" << color_normal;

	// using ints instead of floats -- easier for equality
	std::array<Vector<int, 3>, 3> es = {
	    {
         {1, 0, 0},
         {0, 1, 0},
         {0, 0, 1},
	     }
    };
	std::array<Vector<int, 3>, 2> vs = {
	    {
         {1, 2, 3},
         {0, 10, -100},
	     }
    };
	std::array<Vector<int, 0>, 0> os = {{}};

	std::array<int, 3> coef1 = {10 * 10, -2 * 10, int(0.5 * 10)};
	std::array<int, 2> coef2 = {10 * 10, -2 * 10};
	std::array<int, 0> coef3 = {};

	Vector<int, 3> r1 = {100, -20, 5};
	Vector<int, 3> r2 = {100, 0, 2300};
	Vector<int, 0> r3 = {};

	// scalars
	auto res = linear_combination(es, coef1);
	cout << "linear_combination(\n"
	     << Vector<Vector<int, 3>, 3>(es) << ", " << Vector<int, 3>(coef1)
	     << ")"
	     << " =\n"
	     << res << " -> " << show_ok(res == r1) << endl;
	res = linear_combination(vs, coef2);
	cout << "linear_combination(\n"
	     << Vector<Vector<int, 3>, 2>(vs) << ", " << Vector<int, 2>(coef2)
	     << ")"
	     << " =\n"
	     << res << " -> " << show_ok(res == r2) << endl;
	auto ores = linear_combination(os, coef3);
	cout << "linear_combination(\n"
	     << Vector<Vector<int, 0>, 0>(os) << ", " << Vector<int, 0>(coef3)
	     << ")"
	     << " =\n"
	     << ores << " -> " << show_ok(ores == r3) << endl;
}

// exercise 02 (lerp)
void test_lerp()
{
	cout << color_yellow << "=== test_lerp ===\n" << color_normal;
	float a, b, t, r, res;

	// scalars
	a   = 0;
	b   = 1;
	t   = 0;
	r   = 0;
	res = lerp(a, b, t);
	cout << "lerp(\n"
	     << a << ", " << b << ", " << t << ")"
	     << " =\n"
	     << res << " -> " << show_ok(std::abs(r - res) < 0.00001) << endl;
	a   = 0;
	b   = 1;
	t   = 1;
	r   = 1;
	res = lerp(a, b, t);
	cout << "lerp(\n"
	     << a << ", " << b << ", " << t << ")"
	     << " =\n"
	     << res << " -> " << show_ok(std::abs(r - res) < 0.00001) << endl;
	a   = 0;
	b   = 1;
	t   = 0.5;
	r   = 0.5;
	res = lerp(a, b, t);
	cout << "lerp(\n"
	     << a << ", " << b << ", " << t << ")"
	     << " =\n"
	     << res << " -> " << show_ok(std::abs(r - res) < 0.00001) << endl;
	a   = 21;
	b   = 42;
	t   = 0.3;
	r   = 27.3;
	res = lerp(a, b, t);
	cout << "lerp(\n"
	     << a << ", " << b << ", " << t << ")"
	     << " =\n"
	     << res << " -> " << show_ok(std::abs(r - res) < 0.00001) << endl;

	// vectors
	Vector<float, 2> va   = {2, 1};
	Vector<float, 2> vb   = {4, 2};
	t                     = 0.3;
	Vector<float, 2> vr   = {2.6, 1.3};
	const auto       vres = lerp(va, vb, t);
	const auto       diff = vr - vres;
	bool             cond = std::all_of(diff.begin(), diff.end(),
	                                    [](float f) { return std::abs(f) < 0.00001; });
	cout << "lerp(\n"
	     << va << ", " << vb << ", " << t << ")"
	     << " =\n"
	     << vres << " -> " << show_ok(cond) << endl;

	// matrices
	Matrix<float, 2, 2> ma = {
	    {2, 1},
        {3, 4}
    };
	Matrix<float, 2, 2> mb = {
	    {20, 10},
        {30, 40}
    };
	t                      = 0.5;
	Matrix<float, 2, 2> mr = {
	    {  11, 5.5},
        {16.5,  22}
    };
	const auto mres  = lerp(ma, mb, t);
	const auto mdiff = mr - mres;
	cond             = true;
	for (int i = 0; i < 2; i++) {
		cond &= std::all_of(mdiff[i].begin(), mdiff[i].end(),
		                    [](float f) { return std::abs(f) < 0.00001; });
	}
	cout << "lerp(\n"
	     << ma << ", " << mb << ", " << t << ")"
	     << " =\n"
	     << mres << " -> " << show_ok(cond) << endl;
}

// exercise 03 (dot product)
void test_dot()
{
	cout << color_yellow << "=== test_dot ===\n" << color_normal;

	{
		Vector<int, 2> a   = {0, 0};
		Vector<int, 2> b   = {1, 1};
		auto           r   = 0;
		const auto     res = dot(a, b);
		cout << "dot(\n"
		     << a << ", " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		Vector<int, 2> a   = {1, 1};
		Vector<int, 2> b   = {1, 1};
		auto           r   = 2;
		const auto     res = dot(a, b);
		cout << "dot(\n"
		     << a << ", " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		Vector<int, 2> a   = {-1, 6};
		Vector<int, 2> b   = {3, 2};
		auto           r   = 9;
		const auto     res = dot(a, b);
		cout << "dot(\n"
		     << a << ", " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		Vector<int, 3> a   = {-1, 6, 5};
		Vector<int, 3> b   = {3, 2, 2};
		auto           r   = (-1 * 3 + 6 * 2 + 5 * 2);
		const auto     res = dot(a, b);
		cout << "dot(\n"
		     << a << ", " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
}

// exercise 04 (norm)
void test_norm()
{
	cout << color_yellow << "=== test_norm ===\n" << color_normal;

	{
		Vector<float, 2> v   = {0, 2};
		const auto       r   = 2.f;
		const float      res = norm_1(v);
		cout << "norm_1(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 2> v   = {0, 2};
		const auto       r   = 2.f;
		const float      res = norm(v);
		cout << "norm(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 2> v   = {0, 2};
		const auto       r   = 2.f;
		const float      res = norm_inf(v);
		cout << "norm_inf(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}

	{
		Vector<float, 2> v   = {-3, 2};
		const auto       r   = 5.f;
		const float      res = norm_1(v);
		cout << "norm_1(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 2> v   = {-3, 2};
		const auto       r   = std::sqrt(13.f);
		const float      res = norm(v);
		cout << "norm(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 2> v   = {-3, 2};
		const auto       r   = 3.f;
		const float      res = norm_inf(v);
		cout << "norm_inf(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}

	{
		Vector<float, 3> v   = {0, 0, 0};
		const auto       r   = 0.f;
		const float      res = norm_1(v);
		cout << "norm_1(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 3> v   = {0, 0, 0};
		const auto       r   = 0.f;
		const float      res = norm(v);
		cout << "norm(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 3> v   = {0, 0, 0};
		const auto       r   = 0.f;
		const float      res = norm_inf(v);
		cout << "norm_inf(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}

	{
		Vector<float, 3> v   = {1, 2, 3};
		const auto       r   = 6.f;
		const float      res = norm_1(v);
		cout << "norm_1(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 3> v   = {1, 2, 3};
		const auto       r   = std::sqrt(14.f);
		const float      res = norm(v);
		cout << "norm(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 3> v   = {1, 2, 3};
		const auto       r   = 3.f;
		const float      res = norm_inf(v);
		cout << "norm_inf(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}

	{
		Vector<float, 2> v   = {-1, -2};
		const auto       r   = 3.f;
		const float      res = norm_1(v);
		cout << "norm_1(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 2> v   = {-1, -2};
		const auto       r   = std::sqrt(5.f);
		const float      res = norm(v);
		cout << "norm(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 2> v   = {-1, -2};
		const auto       r   = 2.f;
		const float      res = norm_inf(v);
		cout << "norm_inf(\n"
		     << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
}

// exercise 05 (cosine)
void test_angle_cos()
{
	cout << color_yellow << "=== test_angle_cos ===\n" << color_normal;

	{
		Vector<float, 2> u   = {1, 0};
		Vector<float, 2> v   = {1, 0};
		const auto       r   = 1.f;
		const float      res = angle_cos(u, v);
		cout << "angle_cos(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 2> u   = {1, 0};
		Vector<float, 2> v   = {0, 1};
		const auto       r   = 0.f;
		const float      res = angle_cos(u, v);
		cout << "angle_cos(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 2> u   = {-1, 1};
		Vector<float, 2> v   = {1, -1};
		const auto       r   = -1.f;
		const float      res = angle_cos(u, v);
		cout << "angle_cos(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 2> u   = {2, 1};
		Vector<float, 2> v   = {4, 2};
		const auto       r   = 1.f;
		const float      res = angle_cos(u, v);
		cout << "angle_cos(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 3> u   = {1, 2, 3};
		Vector<float, 3> v   = {4, 5, 6};
		const auto       r   = 0.974631846f;
		const float      res = angle_cos(u, v);
		cout << "angle_cos(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 3> u   = {0, 2, 2};
		Vector<float, 3> v   = {0, -2, -2};
		const auto       r   = -1.f;
		const float      res = angle_cos(u, v);
		cout << "angle_cos(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
}

// exercise 06 (cross product)
void test_cross_product()
{
	cout << color_yellow << "=== test_cross_product ===\n" << color_normal;

	{
		Vector<float, 3>       u   = {0, 0, 1};
		Vector<float, 3>       v   = {1, 0, 0};
		const Vector<float, 3> r   = {0, 1, 0};
		const auto             res = cross_product(u, v);
		cout << "cross_product(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 3>       u   = {1, 2, 3};
		Vector<float, 3>       v   = {4, 5, 6};
		const Vector<float, 3> r   = {-3, 6, -3};
		const auto             res = cross_product(u, v);
		cout << "cross_product(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<float, 3>       u   = {4, 2, -3};
		Vector<float, 3>       v   = {-2, -5, 16};
		const Vector<float, 3> r   = {17, -58, -16};
		const auto             res = cross_product(u, v);
		cout << "cross_product(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		Vector<int, 3>       u   = {4, 2, -3};
		Vector<int, 3>       v   = {-2, -5, 16};
		const Vector<int, 3> r   = {17, -58, -16};
		const auto           res = cross_product(u, v);
		cout << "cross_product(\n"
		     << u << ", " << v << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
}

// exercise 07 (matrix/vector multiplication)
void test_multiplication()
{
	cout << color_yellow << "=== test_multiplication ===\n" << color_normal;

	{
		const Matrix<int, 2, 2> a = {
		    {1, 0},
            {0, 1}
        };
		const Vector<int, 2> b   = {4, 2};
		const Vector<int, 2> r   = {4, 2};
		const auto           res = a * b;
		cout << "(\n"
		     << a << " * " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<int, 2, 2> a = {
		    {2, 0},
            {0, 2}
        };
		const Vector<int, 2> b   = {4, 2};
		const Vector<int, 2> r   = {8, 4};
		const auto           res = a * b;
		cout << "(\n"
		     << a << " * " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<int, 2, 2> a = {
		    { 2, -2},
            {-2,  2}
        };
		const Vector<int, 2> b   = {4, 2};
		const Vector<int, 2> r   = {4, -4};
		const auto           res = a * b;
		cout << "(\n"
		     << a << " * " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		Matrix<int, 2, 2> a = {
		    {1, 0},
            {0, 1}
        };
		Matrix<int, 2, 2> b = {
		    {1, 0},
            {0, 1}
        };
		const Matrix<int, 2, 2> r = {
		    {1, 0},
            {0, 1}
        };
		const auto res = a * b;
		cout << "(\n"
		     << a << " * " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		Matrix<int, 2, 2> a = {
		    {1, 0},
            {0, 1}
        };
		Matrix<int, 2, 2> b = {
		    {2, 1},
            {4, 2}
        };
		const Matrix<int, 2, 2> r = {
		    {2, 1},
            {4, 2}
        };
		const auto res = a * b;
		cout << "(\n"
		     << a << " * " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		Matrix<int, 2, 2> a = {
		    {3, -5},
            {6,  8}
        };
		Matrix<int, 2, 2> b = {
		    {2, 1},
		    {4, 2},
		};
		const Matrix<int, 2, 2> r = {
		    {-14, -7},
		    { 44, 22},
		};
		const auto res = a * b;
		cout << "(\n"
		     << a << " * " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		Matrix<int, 2, 3> a = {
		    {1, 2, 3},
            {4, 5, 6}
        };
		Matrix<int, 3, 2> b = {
		    { 7,  8},
		    { 9, 10},
		    {11, 12},
		};
		const Matrix<int, 2, 2> r = {
		    { 58,  64},
		    {139, 154},
		};
		const auto res = a * b;
		cout << "(\n"
		     << a << " * " << b << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
}

// exercise 08 (trace of square matrix)
void test_trace()
{
	cout << color_yellow << "=== test_trace ===\n" << color_normal;

	{
		const Matrix<int, 2, 2> m = {
		    {1, 0},
		    {0, 1},
		};
		const int  r   = 2;
		const auto res = trace(m);
		cout << "trace(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<int, 3, 3> m = {
		    { 2, -5, 0},
		    { 4,  3, 7},
		    {-2,  3, 4},
		};
		const int  r   = 9;
		const auto res = trace(m);
		cout << "trace(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<int, 3, 3> m = {
		    {-2,  -8, 4},
		    { 1, -23, 4},
		    { 0,   6, 4},
		};
		const int  r   = -21;
		const auto res = trace(m);
		cout << "trace(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
}

// exercise 09 (tranpose of matrix)
void test_transpose()
{
	cout << color_yellow << "=== test_transpose ===\n" << color_normal;

	{
		const Matrix<int, 2, 3> m = {
		    {-2,  -8, 3},
		    { 1, -23, 4},
		};
		const Matrix<int, 3, 2> r = {
		    {-2,   1},
		    {-8, -23},
		    { 3,   4},
		};
		const auto res = transpose(m);
		cout << "transpose(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<int, 1, 1> m   = {{-2}};
		const Matrix<int, 1, 1> r   = {{-2}};
		const auto              res = transpose(m);
		cout << "transpose(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<int, 2, 2> m = {
		    {-2,  -8},
		    { 1, -23},
		};
		const Matrix<int, 2, 2> r = {
		    {-2,   1},
		    {-8, -23},
		};
		const auto res = transpose(m);
		cout << "transpose(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
}

// exercise 10 (reduced row echelon form)
void test_row_echelon()
{
	cout << color_yellow << "=== test_row_echelon ===\n" << color_normal;

	{
		const Matrix<float, 3, 3> m = {
		    {1, 0, 0},
		    {0, 1, 0},
		    {0, 0, 1},
		};
		const Matrix<float, 3, 3> r = {
		    {1, 0, 0},
		    {0, 1, 0},
		    {0, 0, 1},
		};
		const auto res = row_echelon(m);
		cout << "row_echelon(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 2, 2> m = {
		    {1, 2},
		    {3, 4},
		};
		const Matrix<float, 2, 2> r = {
		    {1, 0},
		    {0, 1},
		};
		const auto res = row_echelon(m);
		cout << "row_echelon(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 2, 2> m = {
		    {1, 2},
		    {2, 4},
		};
		const Matrix<float, 2, 2> r = {
		    {1, 2},
		    {0, 0},
		};
		const auto res = row_echelon(m);
		cout << "row_echelon(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 3, 5> m = {
		    {8,   5, -2, 4, 28},
		    {4, 2.5, 20, 4, -4},
		    {8,   5,  1, 4, 17},
		};
		const Matrix<float, 3, 5> r = {
		    {1, 0.625, 0, 0, -12. - 2. / 30. - 0.1},
		    {0,     0, 1, 0,         -3. - 2. / 3.},
		    {0,     0, 0, 1,                  29.5},
		};
		const auto res = row_echelon(m);
		cout << "row_echelon(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		// already in row echelon form
		const Matrix<float, 3, 5> m = {
		    {1, 0.625, 0, 0, -12. - 2. / 30. - 0.1},
		    {0,     0, 1, 0,         -3. - 2. / 3.},
		    {0,     0, 0, 1,                  29.5},
		};
		const Matrix<float, 3, 5> r = {
		    {1, 0.625, 0, 0, -12. - 2. / 30. - 0.1},
		    {0,     0, 1, 0,         -3. - 2. / 3.},
		    {0,     0, 0, 1,                  29.5},
		};
		const auto res = row_echelon(m);
		cout << "row_echelon(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 5, 3> m = {
		    { 6, -9,  1},
            { 1,  0,  0},
            {-2,  1, 70},
            { 0,  4, -5},
            { 5,  8,  1},
		};
		const Matrix<float, 5, 3> r = {
		    {1, 0, 0},
            {0, 1, 0},
            {0, 0, 1},
            {0, 0, 0},
            {0, 0, 0},
		};
		const auto res = row_echelon(m);
		cout << "row_echelon(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 2, 2> m = {
		    {0, 0},
		    {0, 0},
		};
		const Matrix<float, 2, 2> r = {
		    {0, 0},
		    {0, 0},
		};
		const auto res = row_echelon(m);
		cout << "row_echelon(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 2, 2> m = {
		    {1, 1},
		    {1, 1},
		};
		const Matrix<float, 2, 2> r = {
		    {1, 1},
		    {0, 0},
		};
		const auto res = row_echelon(m);
		cout << "row_echelon(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 1, 1> m = {
		    {10},
		};
		const Matrix<float, 1, 1> r = {
		    {1},
		};
		const auto res = row_echelon(m);
		cout << "row_echelon(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
}

// exercise 11 (determinant of square matrix)
void test_determinant()
{
	cout << color_yellow << "=== test_determinant ===\n" << color_normal;

	{
		const Matrix<float, 3, 3> m = {
		    {0, 2, 22. / 3},
		    {4, 2,       1},
		    {2, 7,       9},
		};
		float      r   = 108;
		const auto res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 2, 2> m = {
		    {1, 2},
		    {3, 4},
		};
		float      r   = -2;
		const auto res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 2, 2> m = {
		    { 1, -1},
		    {-1,  1},
		};
		float      r   = 0;
		const auto res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 3, 3> m = {
		    {2, 0, 0},
		    {0, 2, 0},
		    {0, 0, 2},
		};
		float      r   = 8;
		const auto res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 3, 3> m = {
		    {8, 5, -2},
		    {4, 7, 20},
		    {7, 6,  1},
		};
		float      r   = -174;
		const auto res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 1, 1> m = {
		    {-8},
		};
		float      r   = -8;
		const auto res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 0, 0> m   = {};
		float                     r   = 1;
		const auto                res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 4, 4> m = {
		    { 8,   5, -2, 4},
		    { 4, 2.5, 20, 4},
		    { 8,   5,  1, 4},
		    {28,  -4, 17, 1},
		};
		float      r   = 1032;
		const auto res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 3, 3> m = {
		    {1, 1, 1},
		    {0, 0, 0},
		    {0, 0, 0},
		};
		float      r   = 0;
		const auto res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<float, 4, 4> m = {
		    { 8,   5, -2, 4},
		    { 4, 2.5, 20, 4},
		    { 8,   5,  1, 4},
		    {28,  -4, 17, 1},
		};
		float      r   = 1032;
		const auto res = determinant(m);
		cout << "determinant(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
}

// exercise 12 (inverse of square matrix)
void test_inverse()
{
	cout << color_yellow << "=== test_inverse ===\n" << color_normal;

	{
		const Matrix<float, 2, 2> m = {
		    {1, 2},
		    {3, 4},
		};
		const auto res = inverse(m);
		const auto I   = m.identity();
		cout << "inverse(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> "
		     << show_ok(is_close(res * m, I) && is_close(m * res, I)) << endl;
	}
	{
		const Matrix<float, 3, 3> m = {
		    {1, 0, 0},
		    {0, 1, 0},
		    {0, 0, 1},
		};
		const auto res = inverse(m);
		const auto I   = m.identity();
		cout << "inverse(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> "
		     << show_ok(is_close(res * m, I) && is_close(m * res, I)) << endl;
	}
	{
		const Matrix<float, 3, 3> m = {
		    {2, 0, 0},
		    {0, 2, 0},
		    {0, 0, 2},
		};
		const auto res = inverse(m);
		const auto I   = m.identity();
		cout << "inverse(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> "
		     << show_ok(is_close(res * m, I) && is_close(m * res, I)) << endl;
	}
	{
		const Matrix<float, 3, 3> m = {
		    {8, 5, -2},
		    {4, 7, 20},
		    {7, 6,  1},
		};
		const auto res = inverse(m);
		const auto I   = m.identity();
		cout << "inverse(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> "
		     << show_ok(is_close(res * m, I) && is_close(m * res, I)) << endl;
	}
	{
		bool                      throwed = false;
		const Matrix<float, 2, 2> m       = {
            {1, 2},
            {2, 4},
        };
		try {
			inverse(m);
		} catch (const std::runtime_error&) {
			throwed = true;
		}
		cout << "inverse(\n"
		     << m << ")"
		     << " =\n"
		     << (throwed ? "Exception thrown" : "Not thrown") << " -> "
		     << show_ok(throwed) << endl;
	}
	{
		bool                      throwed = false;
		const Matrix<float, 3, 3> m       = {
            {0, 0, 0},
            {0, 0, 0},
            {0, 0, 0},
        };
		try {
			inverse(m);
		} catch (const std::runtime_error&) {
			throwed = true;
		}
		cout << "inverse(\n"
		     << m << ")"
		     << " =\n"
		     << (throwed ? "Exception thrown" : "Not thrown") << " -> "
		     << show_ok(throwed) << endl;
	}
	{
		const Matrix<float, 1, 1> m = {
		    {2},
		};
		const auto res = inverse(m);
		const auto I   = m.identity();
		cout << "inverse(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> "
		     << show_ok(is_close(res * m, I) && is_close(m * res, I)) << endl;
	}
	{
		const Matrix<float, 0, 0> m   = {};
		const auto                res = inverse(m);
		const auto                I   = m.identity();
		cout << "inverse(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> "
		     << show_ok(is_close(res * m, I) && is_close(m * res, I)) << endl;
	}
}

// exercise 13 (rank of matrix)
void test_rank()
{
	cout << color_yellow << "=== test_rank ===\n" << color_normal;

	{
		const Matrix<float, 3, 3> m = {
		    {1, 0, 0},
		    {0, 1, 0},
		    {0, 0, 1},
		};
		const auto r   = 3u;
		const auto res = rank(m);
		cout << "rank(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<float, 3, 4> m = {
		    { 1, 2, 0, 0},
		    { 2, 4, 0, 0},
		    {-1, 2, 1, 1},
		};
		const auto r   = 2u;
		const auto res = rank(m);
		cout << "rank(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<float, 4, 3> m = {
		    { 8,  5, -2},
		    { 4,  7, 20},
		    { 7,  6,  1},
		    {21, 18,  7},
		};
		const auto r   = 3u;
		const auto res = rank(m);
		cout << "rank(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<float, 4, 3> m = {
		    {0, 0, 0},
		    {0, 0, 0},
		    {0, 0, 0},
		    {0, 0, 0},
		};
		const auto r   = 0u;
		const auto res = rank(m);
		cout << "rank(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<float, 2, 2> m = {
		    {1, 1},
		    {1, 1},
		};
		const auto r   = 1u;
		const auto res = rank(m);
		cout << "rank(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<float, 1, 1> m = {
		    {4},
		};
		const auto r   = 1u;
		const auto res = rank(m);
		cout << "rank(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Matrix<float, 0, 0> m   = {};
		const auto                r   = 0u;
		const auto                res = rank(m);
		cout << "rank(\n"
		     << m << ")"
		     << " =\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
}

// exercise 15 (linear algebra using complex numbers)
void test_complex()
{
	cout << color_yellow << "=== test_complex ===\n" << color_normal;

	using std::complex;
	using namespace std::complex_literals;

	{
		const Vector<complex<int>, 3> v1  = {2, 2, 2};
		const Vector<complex<int>, 3> v2  = {1i, 3, -5};
		const Vector<complex<int>, 3> r   = {2. + 1i, 5, -3};
		const auto                    res = v1 + v2;
		cout << v1 << "+\n"
		     << v2 << "=\n"
		     << res << " -> " << show_ok(r == res) << endl;
	}
	{
		std::array<complex<double>, 3>            coef = {10i, -2i, -3. + 1i};
		std::array<Vector<complex<double>, 3>, 3> es   = {
            {
             {1i, 2. + 1i, -4},
             {0, 3, -2},
             {5. + 7i, -1i, 1},
             }
        };

		Vector<complex<double>, 3> r =
		    coef[0] * es[0] + coef[1] * es[1] + coef[2] * es[2];

		// scalars
		auto res = linear_combination(es, coef);
		cout << "linear_combination(\n"
		     << Vector<Vector<complex<double>, 3>, 3>(es) << ", "
		     << Vector<complex<double>, 3>(coef) << ")"
		     << " =\n"
		     << res << " -> " << show_ok(is_close(res, r)) << endl;
	}
	{
		const complex<double> c1 = 1i;
		const complex<double> c2 = 2. - 5i;
		const double          d  = 0.5;

		const complex<double> r   = (1i + (2. - 5i)) / 2.;
		const auto            res = lerp(c1, c2, d);

		cout << "lerp(\n"
		     << c1 << ", " << c2 << ", " << d << ")"
		     << " = " << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const Vector<complex<double>, 2> v1 = {1i, 2. - 2i};
		const Vector<complex<double>, 2> v2 = {3, -3. - 3i};

		const complex<double> r   = 1i * 3. + (2. - 2i) * (-3. - 3i);
		const auto            res = dot(v1, v2);

		cout << "dot(\n"
		     << v1 << ", " << v2 << ")"
		     << " = " << res << " -> " << show_ok(r == res) << endl;
	}
	{
		const complex<double> c1 = 2;
		const complex<double> c2 = -3i;
		const complex<double> c3 = 2. - 3i;

		const float r1 = 2;
		const float r2 = 3;
		const float r3 = sqrtf(4 + 9);
		// std::complex has std::norm attached to it...
		const auto res1 = ::norm(c1);
		const auto res2 = ::norm(c2);
		const auto res3 = ::norm(c3);

		cout << "norm(\n"
		     << c1 << ")"
		     << " = " << res1 << " -> " << show_ok(is_close(r1, res1)) << endl;
		cout << "norm(\n"
		     << c2 << ")"
		     << " = " << res2 << " -> " << show_ok(is_close(r2, res2)) << endl;
		cout << "norm(\n"
		     << c3 << ")"
		     << " = " << res3 << " -> " << show_ok(is_close(r3, res3)) << endl;
	}
	{
		const complex<double> c1 = 2;
		const complex<double> c2 = -3i;
		const complex<double> c3 = 2. - 3i;

		const float r1 = 2;
		const float r2 = 3;
		const float r3 = 5;
		// std::complex has std::norm attached to it...
		const auto res1 = ::norm_1(c1);
		const auto res2 = ::norm_1(c2);
		const auto res3 = ::norm_1(c3);

		cout << "norm_1(\n"
		     << c1 << ")"
		     << " = " << res1 << " -> " << show_ok(is_close(r1, res1)) << endl;
		cout << "norm_1(\n"
		     << c2 << ")"
		     << " = " << res2 << " -> " << show_ok(is_close(r2, res2)) << endl;
		cout << "norm_1(\n"
		     << c3 << ")"
		     << " = " << res3 << " -> " << show_ok(is_close(r3, res3)) << endl;
	}
	{
		const complex<double> c1 = 2;
		const complex<double> c2 = -3i;
		const complex<double> c3 = 2. - 3i;

		const float r1 = 2;
		const float r2 = 3;
		const float r3 = 3;
		// std::complex has std::norm attached to it...
		const auto res1 = ::norm_inf(c1);
		const auto res2 = ::norm_inf(c2);
		const auto res3 = ::norm_inf(c3);

		cout << "norm_inf(\n"
		     << c1 << ")"
		     << " = " << res1 << " -> " << show_ok(is_close(r1, res1)) << endl;
		cout << "norm_inf(\n"
		     << c2 << ")"
		     << " = " << res2 << " -> " << show_ok(is_close(r2, res2)) << endl;
		cout << "norm_inf(\n"
		     << c3 << ")"
		     << " = " << res3 << " -> " << show_ok(is_close(r3, res3)) << endl;
	}
	{
		const Vector<complex<double>, 1> c1 = {1};
		const Vector<complex<double>, 1> c2 = {-1i};

		const auto r   = 0. - 1i;
		const auto res = angle_cos(c1, c2);

		cout << "anlge_cos(\n"
		     << c1 << ", " << c2 << ")"
		     << " = " << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Vector<complex<double>, 2> c1 = {1, 0};
		const Vector<complex<double>, 2> c2 = {0, 1};

		const auto r   = 0i;
		const auto res = angle_cos(c1, c2);

		cout << "anlge_cos(\n"
		     << c1 << ", " << c2 << ")"
		     << " = " << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Vector<complex<double>, 2> c1 = {2, 2i};
		const Vector<complex<double>, 2> c2 = {1i, 1};

		const auto r   = 1i;
		const auto res = angle_cos(c1, c2);

		cout << "anlge_cos(\n"
		     << c1 << ", " << c2 << ")"
		     << " = " << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Vector<complex<double>, 2> c1 = {2, 2i};
		const Vector<complex<double>, 2> c2 = {1, 1i};

		const auto r   = 0i;
		const auto res = angle_cos(c1, c2);

		cout << "anlge_cos(\n"
		     << c1 << ", " << c2 << ")"
		     << " = " << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Vector<complex<double>, 3> c1 = {2, 2i, 2. + 2i};
		const Vector<complex<double>, 3> c2 = {1, 1i, 1. + 1i};

		const Vector<complex<double>, 3> r   = {0i, 0i, 0i};
		const auto                       res = cross_product(c1, c2);

		cout << "cross_product(\n"
		     << c1 << ", " << c2 << ")"
		     << " = " << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Vector<complex<double>, 3> c1 = {2, -2i, 2. + 2i};
		const Vector<complex<double>, 3> c2 = {1, 1i, 1. + 1i};

		const Vector<complex<double>, 3> r   = {4. - 4i, 0i, 4i};
		const auto                       res = cross_product(c1, c2);

		cout << "cross_product(\n"
		     << c1 << ", " << c2 << ")"
		     << " = " << res << " -> " << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<complex<double>, 2, 2> m = {
		    { 2, 2i},
		    {-2, 4i},
		};
		const Vector<complex<double>, 2> v = {1. + 1i, 1. - 1i};

		const Vector<complex<double>, 2> r = {
		    2. * (1. + 1i) + 2i * (1. - 1i),
		    -2. * (1. + 1i) + 4i * (1. - 1i),
		};
		const auto res = m * v;

		cout << m << " * " << v << " = " << res << " -> "
		     << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<complex<double>, 2, 2> m1 = {
		    { 1, 2i},
		    {-2, 4i},
		};
		const Matrix<complex<double>, 2, 2> m2 = {
		    {  2,  2i},
		    {-2i, -1i},
		};

		const Matrix<complex<double>, 2, 2> r = {
		    { (1 * 2. + 2i * -2i),  (1. * 2i + 2i * -1i)},
		    {(-2. * 2 + 4i * -2i), (-2. * 2i + 4i * -1i)},
		};
		const auto res = m1 * m2;

		cout << m1 << " * " << m2 << " = " << res << " -> "
		     << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<complex<double>, 3, 3> m = {
		    { 1, 2i, 2. - 3i},
		    {-2, 4i, 2. - 3i},
		    {-2, 4i, 2. - 3i},
		};
		const auto r   = 1. + 4i + 2. - 3i;
		const auto res = trace(m);

		cout << "trace(" << m << ") = " << res << " -> "
		     << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<complex<double>, 3, 3> m = {
		    { 1, 2i, 1. - 3i},
		    {-2, 4i, 2. - 3i},
		    {-2, 4i, 2. - 3i},
		};
		const Matrix<complex<double>, 3, 3> r = {
		    {      1,      -2,      -2},
		    {     2i,      4i,      4i},
		    {1. - 3i, 2. - 3i, 2. - 3i},
		};
		const auto res = transpose(m);

		cout << "transpose(" << m << ") = " << res << " -> "
		     << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<complex<double>, 3, 3> m = {
		    { 1, 2i, 1. - 3i},
		    {-2, 4i, 2. - 3i},
		    {-2, 4i, 2. - 3i},
		};
		const Matrix<complex<double>, 3, 3> r = {
		    {1, 0,        -0.75i},
		    {0, 1, -1.125 - 0.5i},
		    {0, 0,             0},
		};
		const auto res = row_echelon(m);

		cout << "transpose(" << m << ") = " << res << " -> "
		     << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<complex<double>, 3, 3> m = {
		    { 1, 2i, 1. - 3i},
		    {-2, 4i, 2. - 3i},
		    {-2, 4i, 2. - 3i},
		};
		const auto det_bl = (-2. * 4i - 4i * -2.);
		const auto det_br = (4i * (2. - 3i) - (2. - 3i) * 4i);
		const auto det_bm = (-2. * (2. - 3i) - (2. - 3i) * -2.);
		const auto r      = 1. * det_br - 2i * det_bm + (1. - 3i) * det_bl;
		const auto res    = determinant(m);

		cout << "determinant(" << m << ") = " << res << " -> "
		     << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<complex<double>, 2, 2> m = {
		    {2i, 1. - 3i},
		    {4i, 2. - 3i},
		};
		const auto r   = 2i * (2. - 3i) - (1. - 3i) * 4i;
		const auto res = determinant(m);

		cout << "determinant(" << m << ") = " << res << " -> "
		     << show_ok(is_close(r, res)) << endl;
	}
	{
		const Matrix<complex<double>, 3, 3> m = {
		    { 1i, 2i, 1. - 3i},
		    {-2i,  0, 2. - 1i},
		    {-2i, 4i,      3i},
		};
		const auto res = inverse(m);

		cout << "inverse(" << m << ") = " << res << " -> "
		     << show_ok(is_close(m.identity(), m * res) &&
		                is_close(m.identity(), res * m))
		     << endl;
	}
	{
		const Matrix<complex<double>, 2, 2> m = {
		    { 1, 2i},
		    {-2, 4i},
		};
		const auto res = inverse(m);

		cout << "inverse(" << m << ") = " << res << " -> "
		     << show_ok(is_close(m.identity(), m * res) &&
		                is_close(m.identity(), res * m))
		     << endl;
	}
	{
		const Matrix<complex<double>, 3, 3> m = {
		    { 1,  2i, 3. + 1i},
		    { 2,  4i,      1i},
		    {-1, -2i,      10},
		};
		const auto          res = rank(m);
		const decltype(res) r   = 2;

		cout << "rank(" << m << ") = " << res << " -> " << show_ok(res == r)
		     << endl;
	}
	{
		const Matrix<complex<double>, 2, 2> m = {
		    { 1, 2i},
		    {-2, 4i},
		};
		const auto          res = rank(m);
		const decltype(res) r   = 2;

		cout << "rank(" << m << ") = " << res << " -> " << show_ok(res == r)
		     << endl;
	}
}

int main()
{
	// mandatory tests
	test_scale_add_sub();
	test_linear_combination();
	test_lerp();
	test_dot();
	test_norm();
	test_angle_cos();
	test_cross_product();
	test_multiplication();
	test_trace();
	test_transpose();
	test_row_echelon();
	test_determinant();
	test_inverse();
	test_rank();

	// bonuses
	// skipping ex14 - the display program does not work (at east on linux)
	test_complex(); // ex15

	// print summary of tests
	print_results();
}
