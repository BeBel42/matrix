#ifndef MATRIX_MULTIPLICATION_H_
#define MATRIX_MULTIPLICATION_H_

#include "../ex02/fma_overloads.hpp"
#include "../inc/matrix.hpp"
#include "../inc/vector.hpp"
#include <cmath>

/*
  matrix * matrix -> matrix
  time complexity: O(nmp)
  space complexity: O(mp)
*/
template <typename K, std::size_t m, std::size_t n, std::size_t p>
Matrix<K, m, p> operator*(const Matrix<K, m, n>& a, const Matrix<K, n, p>& b)
{
	auto r = Matrix<K, m, p>();
	for (decltype(m) im = 0; im < m; ++im) {
		for (decltype(p) ip = 0; ip < p; ++ip) {
			for (decltype(n) in = 0; in < n; ++in) {
				r[im][ip] = std::fma(a[im][in], b[in][ip], r[im][ip]);
			}
		}
	}
	return r;
}

/*
  matrix * vector -> vector
  time complexity: O(nm)
  space complexity: O(m)
*/
template <typename K, std::size_t m, std::size_t n>
Vector<K, m> operator*(const Matrix<K, m, n>& a, const Vector<K, n>& u)
{
	// O(mn)
	auto r = Vector<K, m>();
	for (decltype(m) im = 0; im < m; ++im) {
		for (decltype(n) in = 0; in < n; ++in) {
			r[im] = std::fma(a[im][in], u[in], r[im]);
		}
	}
	return r;
}

#endif // MATRIX_MULTIPLICATION_H_
