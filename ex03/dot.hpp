#ifndef DOT_H_
#define DOT_H_

#include "../inc/vector.hpp"
#include <cmath>

// returns dot product of two vectors
template <typename K, std::size_t n>
K dot(const Vector<K, n>& a, const Vector<K, n>& b)
{
	auto r = K();
	for (decltype(n) i = 0; i < n; i++) {
		// r += a * b
		r = std::fma(a[i], b[i], r);
	}
	return r;
}

#endif // DOT_H_
