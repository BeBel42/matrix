#ifndef ROW_ECHELON_H_
#define ROW_ECHELON_H_

#include "../inc/matrix.hpp"
#include <optional>

namespace ns_row_echelon
{

// find next leftmost pivot
template <typename K, std::size_t m, std::size_t n>
static std::optional<std::pair<decltype(n), decltype(m)>>
get_leftmost(const Matrix<K, m, n>& mat, const decltype(n) x,
             const decltype(m) y)
{
	// return next non null coordinates
	for (auto ix = x; ix < n; ++ix) {
		for (auto iy = y; iy < m; ++iy) {
			if (mat[iy][ix] != K()) {
				return std::pair<decltype(n), decltype(m)>{ix, iy};
			}
		}
	}
	// nothing found - empty optional
	return {};
}

// recursively compute the rref
template <typename K, std::size_t m, std::size_t n>
static void echelon_r(Matrix<K, m, n>& r, const decltype(n) x,
                      const decltype(m) y)
{
	// find next pivot
	const auto leftmost = get_leftmost(r, x, y);
	// no next pivot - done
	if (!leftmost) {
		return;
	}
	// pivot coordinates {x, y}
	auto lm = *leftmost;

	// swap pivot with corner
	std::swap(r[y], r[lm.second]);
	lm.second = y;

	// set pivot to 1 using elementary row operations
	r[lm.second] /= r[lm.second][lm.first];

	// set all values below to zero using elementary ops
	for (decltype(m) i = lm.second + 1; i < m; ++i) {
		r[i] -= r[lm.second] * r[i][lm.first];
	}

	// set all values above to zero using elementary ops
	// remove this to get the (not reduced) echelon form
	for (auto i = lm.second; i > 0; i--) {
		r[i - 1] -= r[lm.second] * r[i - 1][lm.first];
	}

	// do the same for the next submatrix
	echelon_r(r, lm.first + 1, lm.second + 1);
}

} // namespace ns_row_echelon

/*
  returns reduced row echelon form of matrix

  time complexity: O(n^2)
  space complexity: O(2n) == O(n)
 */
template <typename K, std::size_t m, std::size_t n>
Matrix<K, m, n> row_echelon(const Matrix<K, m, n>& mat)
{
	// copy matrix to return value
	Matrix<K, m, n> r = mat;
	// start recursive functino at first element
	ns_row_echelon::echelon_r(r, 0, 0);
	// r is modified - return it
	return r;
}

#endif // ROW_ECHELON_H_
