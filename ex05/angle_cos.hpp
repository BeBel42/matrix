#ifndef ANGLE_COS_H_
#define ANGLE_COS_H_

#include "../ex03/dot.hpp"
#include "../ex04/norm.hpp"
#include "../inc/vector.hpp"
#include <complex>

// complex numbers
using std::complex;
template <typename K, std::size_t n>
complex<K> angle_cos(const Vector<complex<K>, n>& u,
                     const Vector<complex<K>, n>& v)
{
	return dot(u, v) / complex<K>(norm(u)) / complex<K>(norm(v));
}

template <typename K, std::size_t n>
float angle_cos(const Vector<K, n>& u, const Vector<K, n>& v)
{
	return dot(u, v) / norm(u) / norm(v);
}

#endif // ANGLE_COS_H_
