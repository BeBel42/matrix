#ifndef TRANSPOSE_H_
#define TRANSPOSE_H_

#include "../inc/matrix.hpp"

// tranpsose matrix columns to rows
template <typename K, std::size_t m, std::size_t n>
Matrix<K, n, m> transpose(const Matrix<K, m, n>& mat)
{
	Matrix<K, n, m> r;

	for (decltype(n) i = 0; i < n; ++i) {
		for (decltype(m) j = 0; j < m; ++j) {
			r[i][j] = mat[j][i];
		}
	}
	return r;
}

#endif // TRANSPOSE_H_
