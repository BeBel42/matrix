#ifndef LERP_H_
#define LERP_H_

#include "fma_overloads.hpp"
#include <cmath>

template <typename T>
T lerp(T a, T b, float t)
{
	// t * (b-a) + a
	return std::fma(t, b - a, a);
}

#endif // LERP_H_
