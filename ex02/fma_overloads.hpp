#ifndef FMA_OVERLOADS_H_
#define FMA_OVERLOADS_H_

#include <cmath>
#include <complex>

// overlaod std::fma for complex numers
namespace std
{
template <typename K>
complex<K> fma(complex<K> x, complex<K> y, complex<K> z)
{
	return x * y + z;
}

template <typename K, typename T>
complex<K> fma(T x, complex<K> y, complex<K> z)
{
	return complex<K>(x) * y + z;
}
template <typename K, typename T>
complex<K> fma(complex<K> x, complex<K> y, T z)
{
	return x * y + complex<K>(z);
}
template <typename K, typename T>
complex<K> fma(complex<K> x, double y, T z)
{
	return x * complex<K>(y) + complex<K>(z);
}
} // namespace std

#include "../inc/matrix.hpp"
#include "../inc/vector.hpp"

// overlaod std::fma for Vector and Matrix class
namespace std
{
// overloads
template <typename K, std::size_t n>
auto fma(K x, const Vector<K, n>& y, const Vector<K, n>& z)
{
	Vector<K, n> r;
	for (decltype(n) i = 0; i < n; ++i) {
		r[i] = std::fma(x, y[i], z[i]);
	}
	return r;
}
template <typename K, std::size_t m, std::size_t n>
auto fma(K x, const Matrix<K, m, n>& y, const Matrix<K, m, n>& z)
{
	Matrix<K, m, n> r;
	for (decltype(m) i = 0; i < m; ++i) {
		r[i] = std::fma(x, y[i], z[i]);
	}
	return r;
}

// with inverted parameters
template <typename K, std::size_t n>
auto fma(const Vector<K, n>& y, K x, const Vector<K, n>& z)
{
	return std::fma(x, y, z);
}

template <typename K, std::size_t m, std::size_t n>
auto fma(const Matrix<K, m, n>& y, K x, const Matrix<K, m, n>& z)
{
	return std::fma(x, y, z);
}

} // namespace std

#endif // FMA_OVERLOADS_H_
