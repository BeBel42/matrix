#ifndef VECTOR_H_
#define VECTOR_H_

#include <algorithm>
#include <array>
#include <cstddef>
#include <iostream>

// n length vector of field K
template <typename K, std::size_t n>
class Vector
{
	using array_t   = std::array<K, n>;
	using arr_ini_t = std::initializer_list<K>;
	array_t data;

  public:
	// ctors
	constexpr Vector() = default;
	constexpr Vector(const array_t& array);
	constexpr Vector(const array_t&& array);
	constexpr Vector(const arr_ini_t& array);

	// utils
	constexpr K&       operator[](const auto i);
	constexpr const K& operator[](const auto i) const;

	// iterations
	decltype(auto) begin() noexcept;
	decltype(auto) begin() const noexcept;
	decltype(auto) cbegin() const noexcept;
	decltype(auto) end() noexcept;
	decltype(auto) end() const noexcept;
	decltype(auto) cend() const noexcept;
};

// ctors
template <typename K, std::size_t n>
constexpr Vector<K, n>::Vector(const array_t& array) : data(array){};
template <typename K, std::size_t n>
constexpr Vector<K, n>::Vector(const array_t&& array)
    : data(std::move(array)){};
template <typename K, std::size_t n>
constexpr Vector<K, n>::Vector(const arr_ini_t& array)
{
	auto index = 0u;
	for (auto&& i : array) {
		data[index] = i;
		index++;
	}
}

// utils
template <typename K, std::size_t n>
constexpr K& Vector<K, n>::operator[](const auto i)
{
	return data[i];
};
template <typename K, std::size_t n>
constexpr const K& Vector<K, n>::operator[](const auto i) const
{
	return data[i];
};

// iterations
template <typename K, std::size_t n>
decltype(auto) Vector<K, n>::begin() noexcept
{
	return data.begin();
}
template <typename K, std::size_t n>
decltype(auto) Vector<K, n>::begin() const noexcept
{
	return data.cbegin();
}
template <typename K, std::size_t n>
decltype(auto) Vector<K, n>::cbegin() const noexcept
{
	return data.cbegin();
}
template <typename K, std::size_t n>
decltype(auto) Vector<K, n>::end() noexcept
{
	return data.end();
}
template <typename K, std::size_t n>
decltype(auto) Vector<K, n>::end() const noexcept
{
	return data.cend();
}
template <typename K, std::size_t n>
decltype(auto) Vector<K, n>::cend() const noexcept
{
	return data.cend();
}

// print vector
template <typename K, std::size_t n>
std::ostream& operator<<(std::ostream& o, const Vector<K, n>& vec)
{
	o << '[';
	for (decltype(n) i = 0; i < n; ++i) {
		o << ' ' << vec[i];
	}
	return o << " ]\n";
}

// including + - and * related operators from ex00
#include "../ex00/vector_operators.hpp"

// comparison operators
template <typename K, std::size_t n>
bool operator==(const Vector<K, n>& lhs, const Vector<K, n>& rhs)
{
	for (decltype(n) i = 0; i < n; ++i) {
		if (lhs[i] != rhs[i]) {
			return false;
		}
	}
	return true;
}
template <typename K, std::size_t n>
bool operator!=(const Vector<K, n>& lhs, const Vector<K, n>& rhs)
{
	return !(lhs == rhs);
}

#endif // VECTOR_H_
