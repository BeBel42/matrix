#ifndef MATRIX_H_
#define MATRIX_H_

#include <array>
#include <cstddef>
#include <initializer_list>
#include <ostream>

#include "vector.hpp"

// m x n matrix of field K
template <typename K, std::size_t m, std::size_t n>
class Matrix
{
	// type aliases
	using arr_vec_t = Vector<Vector<K, n>, m>;
	using arr_ini_t = std::initializer_list<Vector<K, n>>;

	// data of matrix
	arr_vec_t data;

  public:
	// ctors
	constexpr Matrix() = default;
	constexpr Matrix(const Vector<K, n>&);
	constexpr Matrix(const Vector<K, n>&&);
	constexpr Matrix(const arr_ini_t& array);
	constexpr Matrix(const arr_vec_t& array);
	constexpr Matrix(const arr_vec_t&& array);

	// utils
	constexpr Vector<K, n>&       operator[](const auto i);
	constexpr const Vector<K, n>& operator[](const auto i) const;

	// get identity of square matrix
	static constexpr Matrix<K, m, n> identity();
};

// ctors
template <typename K, std::size_t m, std::size_t n>
constexpr Matrix<K, m, n>::Matrix(const Vector<K, n>& vec) : data({vec})
{
}
template <typename K, std::size_t m, std::size_t n>
constexpr Matrix<K, m, n>::Matrix(const Vector<K, n>&& vec)
    : data({std::move(vec)})
{
}
template <typename K, std::size_t m, std::size_t n>
constexpr Matrix<K, m, n>::Matrix(const Matrix<K, m, n>::arr_vec_t& array)
    : data(array){};
template <typename K, std::size_t m, std::size_t n>
constexpr Matrix<K, m, n>::Matrix(const arr_vec_t&& array)
    : data(std::move(array)){};
template <typename K, std::size_t m, std::size_t n>
constexpr Matrix<K, m, n>::Matrix(const arr_ini_t& array)
{
	auto index = 0u;
	for (auto&& i : array) {
		data[index] = i;
		index++;
	}
}

// utils
template <typename K, std::size_t m, std::size_t n>
constexpr Vector<K, n>& Matrix<K, m, n>::operator[](const auto i)
{
	return data[i];
};
template <typename K, std::size_t m, std::size_t n>
constexpr const Vector<K, n>& Matrix<K, m, n>::operator[](const auto i) const
{
	return data[i];
};

// print matrix
template <typename K, std::size_t m, std::size_t n>
std::ostream& operator<<(std::ostream& o, const Matrix<K, m, n>& mat)
{
	for (decltype(m) i = 0; i < m; ++i) {
		o << mat[i];
	}
	return o;
}

// get identity of square matrix
template <typename K, std::size_t m, std::size_t n>
constexpr Matrix<K, m, n> Matrix<K, m, n>::identity()
{
	static_assert(m == n);
	auto r = Matrix<K, n, n>();
	for (decltype(n) i = 0; i < n; ++i) {
		r[i][i] = 1;
	}
	return r;
}

// including + - and * related operators from ex00
#include "../ex00/matrix_operators.hpp"

// comparison operators
template <typename K, std::size_t m, std::size_t n>
bool operator==(const Matrix<K, m, n>& lhs, const Matrix<K, m, n>& rhs)
{
	for (decltype(m) i = 0; i < m; ++i) {
		if (lhs[i] != rhs[i]) {
			return false;
		}
	}
	return true;
}
template <typename K, std::size_t m, std::size_t n>
bool operator!=(const Matrix<K, m, n>& lhs, const Matrix<K, m, n>& rhs)
{
	return !(lhs == rhs);
}

// * operator
#include "../ex07/matrix_multiplication.hpp"

#endif // MATRIX_H_
