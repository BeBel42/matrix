#ifndef TRACE_H_
#define TRACE_H_

#include "../inc/matrix.hpp"

// sum of main diagonal of square matrix
template <typename K, std::size_t n>
K trace(const Matrix<K, n, n>& m)
{
	auto r = K();
	for (decltype(n) i = 0; i < n; ++i) {
		r += m[i][i];
	}
	return r;
}

#endif // TRACE_H_
