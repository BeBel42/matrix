#ifndef NORM_H_
#define NORM_H_

#include "../inc/vector.hpp"
#include <algorithm>
#include <cmath>
#include <complex>
#include <numeric>

using std::complex;
using namespace std::complex_literals;

// norm for generic types
template <typename T>
float norm(T k)
{
	return std::abs(k);
}

// norm for complex numbers
template <typename T>
float norm(std::complex<T> k)
{
	// norm ov C is sqrt(z * z.conj()) == sqrt(a^2 + b^2)
	using std::pow;
	return pow(pow(k.real(), 2) + pow(k.imag(), 2), 0.5);
}
template <typename T>
float norm_1(std::complex<T> k)
{
	return norm(k.real()) + norm(k.imag());
}
template <typename T>
float norm_inf(std::complex<T> k)
{
	return std::max(norm(k.real()), norm(k.imag()));
}

// returns euclidian norm of complex vector
// returns manhattan norm of vector
template <typename K, std::size_t n>
float norm_1(const Vector<K, n>& v) noexcept
{
	const auto binary_op = [](const K& init, const K& val) -> K {
		return init + norm(val);
	};
	return std::reduce(v.begin(), v.end(), 0, binary_op);
}

// returns euclidian norm of vector
template <typename K, std::size_t n>
float norm(const Vector<K, n>& v) noexcept
{
	const auto binary_op = [](const auto& init, const auto& val) {
		// val * val + init
		return std::fma(::norm(val), ::norm(val), ::norm(init));
	};
	const auto sum = std::reduce(v.begin(), v.end(), 0, binary_op);
	return std::pow(sum, 0.5);
}

// returns suprenum norm of vector
template <typename K, std::size_t n>
float norm_inf(const Vector<K, n>& v) noexcept
{
	auto biggest = std::abs(n == 0 ? K() : v[0]);
	for (auto i : v) {
		biggest = std::max(norm(i), biggest);
	}
	return biggest;
}

#endif // NORM_H_
