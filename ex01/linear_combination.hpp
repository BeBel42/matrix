#ifndef LINEAR_COMBINATION_H_
#define LINEAR_COMBINATION_H_

#include "../inc/vector.hpp"

/*
  n2 * n == n of pdf
  here it is n2 * (n + n) == 2 * n of pdf (linear, so O(n))
*/
template <typename K, std::size_t n, std::size_t n2>
Vector<K, n> linear_combination(const std::array<Vector<K, n>, n2>& u,
                                const std::array<K, n2>& coefs) noexcept
{
	// zero-initialized vector
	auto r = Vector<K, n>();

	// add all summed scaled vectors into result
	for (decltype(n2) i = 0; i < n2; i++) {
		r += u[i] * coefs[i];
	}

	return r;
}

#endif // LINEAR_COMBINATION_H_
