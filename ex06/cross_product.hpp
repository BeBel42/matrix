#ifndef CROSS_PRODUCT_H_
#define CROSS_PRODUCT_H_

#include "../inc/vector.hpp"

// cross product of two vectors
template <typename K>
Vector<K, 3> cross_product(const Vector<K, 3>& u, const Vector<K, 3>& v)
{
	return {
	    u[1] * v[2] - u[2] * v[1],
	    u[2] * v[0] - u[0] * v[2],
	    u[0] * v[1] - u[1] * v[0],
	};
}

#endif // CROSS_PRODUCT_H_
