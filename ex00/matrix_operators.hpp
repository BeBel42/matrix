#ifndef OPERATORS_H_
#define OPERATORS_H_

// for lsp - ignore error
#include "../inc/matrix.hpp"

// operations
template <typename K, std::size_t m, std::size_t n>
auto& operator+=(Matrix<K, m, n>& lhs, const Matrix<K, m, n>& rhs)
{
	for (auto i = 0u; i < m; ++i) {
		lhs[i] += rhs[i];
	}
	return lhs;
}
template <typename K, std::size_t m, std::size_t n>
auto& operator-=(Matrix<K, m, n>& lhs, const Matrix<K, m, n>& rhs)
{
	for (auto i = 0u; i < m; ++i) {
		lhs[i] -= rhs[i];
	}
	return lhs;
}
template <typename K, std::size_t m, std::size_t n>
auto operator*=(Matrix<K, m, n>& lhs, const K scalar)
{
	for (decltype(m) i = 0; i < m; i++) {
		lhs[i] *= scalar;
	}
	return lhs;
}
template <typename K, std::size_t m, std::size_t n>
auto operator/=(Matrix<K, m, n>& lhs, const K scalar)
{
	for (decltype(m) i = 0; i < m; i++) {
		lhs[i] /= scalar;
	}
	return lhs;
}
// reuse operators to generate others
template <typename K, std::size_t m, std::size_t n>
auto operator*(const K scalar, const Matrix<K, m, n>& rhs)
{
	auto r = rhs;
	return r *= scalar;
}
template <typename K, std::size_t m, std::size_t n>
auto operator*(const Matrix<K, m, n>& lhs, const K scalar)
{
	return scalar * lhs;
}
template <typename K, std::size_t m, std::size_t n>
auto operator/(const Matrix<K, m, n>& lhs, const K scalar)
{
	auto r = lhs;
	return r /= scalar;
}
template <typename K, std::size_t m, std::size_t n>
auto operator+(const Matrix<K, m, n>& lhs, const Matrix<K, m, n>& rhs)
{
	auto r = lhs;
	return r += rhs;
}
template <typename K, std::size_t m, std::size_t n>
auto operator-(const Matrix<K, m, n>& lhs, const Matrix<K, m, n>& rhs)
{
	auto r = lhs;
	return r -= rhs;
}

#endif // OPERATORS_H_
