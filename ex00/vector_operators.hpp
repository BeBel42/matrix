#ifndef VECTOR_OPERATORS_H_
#define VECTOR_OPERATORS_H_

// for lsp - ignore error
#include "../inc/vector.hpp"

// operations
template <typename K, std::size_t n>
auto& operator+=(Vector<K, n>& lhs, const Vector<K, n>& rhs)
{
	for (auto i = 0u; i < n; ++i) {
		lhs[i] += rhs[i];
	}
	return lhs;
}
template <typename K, std::size_t n>
auto& operator-=(Vector<K, n>& lhs, const Vector<K, n>& rhs)
{
	for (auto i = 0u; i < n; ++i) {
		lhs[i] -= rhs[i];
	}
	return lhs;
}
template <typename K, std::size_t n>
auto operator*=(Vector<K, n>& lhs, const K scalar)
{
	std::for_each(lhs.begin(), lhs.end(),
	              [scalar = scalar](K& i) { i *= scalar; });
	return lhs;
}
template <typename K, std::size_t n>
auto operator/=(Vector<K, n>& lhs, const K scalar)
{
	std::for_each(lhs.begin(), lhs.end(),
	              [scalar = scalar](K& i) { i /= scalar; });
	return lhs;
}
// reuse operators to generate others
template <typename K, std::size_t n>
auto operator*(const K scalar, const Vector<K, n>& rhs)
{
	auto r = rhs;
	return r *= scalar;
}
template <typename K, std::size_t n>
auto operator*(const Vector<K, n>& lhs, const K scalar)
{
	auto r = lhs;
	return r *= scalar;
}
template <typename K, std::size_t n>
auto operator/(const Vector<K, n>& lhs, const K scalar)
{
	auto r = lhs;
	return r /= scalar;
}
template <typename K, std::size_t n>
auto operator+(const Vector<K, n>& lhs, const Vector<K, n>& rhs)
{
	auto r = lhs;
	return r += rhs;
}
template <typename K, std::size_t n>
auto operator-(const Vector<K, n>& lhs, const Vector<K, n>& rhs)
{
	auto r = lhs;
	return r -= rhs;
}

#endif // VECTOR_OPERATORS_H_
