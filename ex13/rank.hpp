#ifndef RANK_H_
#define RANK_H_

#include "../ex10/row_echelon.hpp"
#include "../inc/matrix.hpp"
#include "../inc/vector.hpp"

/*
  returns the rank of mxn matrix using the row echelon form algorithm

  time complexity: O(n^2)
  space complexity: O(n)
 */
template <typename K, std::size_t m, std::size_t n>
auto rank(const Matrix<K, m, n>& M) -> decltype(m)
{
	// convert matrix to row echelon form
	const auto row_ech = row_echelon(M);

	// vector representing a zero row
	const auto zero_vector = Vector<K, n>();

	/*
	  count number of non-zero rows in row echolon form
	   in the REF, all zer rows are at the bottom
	*/
	decltype(m) i = 0;
	while (i < m) {
		if (row_ech[i] == zero_vector) {
			break;
		}
		i++;
	}

	// number of non-zero rows are the rank of the matrix
	return i;
}

#endif // RANK_H_
