#ifndef INVERSE_H_
#define INVERSE_H_

#include "../inc/matrix.hpp"
#include "./substitutions.hpp"
#include <optional>

namespace std
{

// isfinite  overlaod for complex numbers
template <typename T>
bool isfinite(const complex<T>& c)
{
	return isfinite(c.real()) && isfinite(c.imag());
}

} // namespace std

namespace ns_inverse
{

// find next leftmost pivot
template <typename K, std::size_t n>
static std::optional<std::pair<decltype(n), decltype(n)>>
get_leftmost(const Matrix<K, n, n>& mat, const decltype(n) x,
             const decltype(n) y)
{
	// return next non null coordinates
	for (auto ix = x; ix < n; ++ix) {
		for (auto iy = y; iy < n; ++iy) {
			if (mat[iy][ix] != K()) {
				return std::pair<decltype(n), decltype(n)>{ix, iy};
			}
		}
	}
	// nothing found - empty optional
	return {};
}

// get full LU decomposition
template <typename K, std::size_t n>
static void LU_decomposition(Matrix<K, n, n>& u, const decltype(n) x,
                             const decltype(n) y, Matrix<K, n, n>& l,
                             Matrix<K, n, n>& p)
{
	// find next pivot
	const auto leftmost = get_leftmost(u, x, y);
	// no next pivot - done
	if (!leftmost) {
		return;
	}
	// pivot coordinates {x, y}
	auto lm = *leftmost;

	// swap pivot with corner
	std::swap(u[y], u[lm.second]);
	// swapping permutation matrix's rows
	std::swap(p[y], p[lm.second]);
	lm.second = y;

	/*
	  set all values below to zero using elementary ops
	  the L matrix is ignored - since its determinant is always 1
	  */
	for (decltype(n) i = lm.second + 1; i < n; ++i) {
		const auto multiplier = (u[i][lm.first] / u[lm.second][lm.first]);
		// equivalent to u[i] -= u[lm.second] * multiplier;
		u[i] = std::fma(u[lm.second], -multiplier, u[i]);
		// filling L matrix
		l[i][lm.first] = multiplier;
	}

	// do the same for the next submatrix
	LU_decomposition(u, lm.first + 1, lm.second + 1, l, p);
}

} // namespace ns_inverse

/*
  Finding the inverse of a matrix using LU decompoosition and matrix
  substitutions

  source:
  https://en.wikipedia.org/wiki/LU_decomposition#Solving_linear_equations

  time complexity: O(n^3 + 2n^2) == O(n^3)
  space complexity: O(n)
 */
template <typename K, std::size_t n>
Matrix<K, n, n> inverse(const Matrix<K, n, n>& A)
{
	// U matrix
	Matrix<K, n, n> U = A;

	// bottom triangle matrix (starts as identity)
	auto L = A.identity();
	// permutation matrix (starts as identity)
	auto P = L;

	// deompose matrix in PA = LU
	ns_inverse::LU_decomposition(U, 0, 0, L, P);

	// LUA^-1 = P*I
	const auto r = back_substitution(U, forward_substitution(L, P));

	/*
	  Check if matrix is valid (if A is not singular)

	  Return this if you prefer getting the real value of the matrix
	  (i.e.: matrix full of nan and inf). I throw here because it is asked
	  in the pdf.
	  */
	if (n != 0 && !std::isfinite(r[0][0])) {
		throw std::runtime_error("Error: matrix is singular");
	}

	return r;
}

#endif // INVERSE_H_
