#ifndef SUBSTITUTIONS_H_
#define SUBSTITUTIONS_H_

#include "../inc/matrix.hpp"
#include "../inc/vector.hpp"

/*
  Implementations of the forward substitution and back substitution for:
  Ux = b; UX = B; Lx = b; LX = B;
  U and L must be upper and lower triangular matrices respectively.
  b and B are a Vector / Matrix.
  x and X are the Vector / Matrix returned by the function to solve the
  equation.
  Source used: https://en.wikipedia.org/wiki/Triangular_matrix
 */

/*
  L must be a lower triangular matrix.
  let x be the return vale, L*x == b

  time complexity: O(n^2)
  space complexity: O(n)
  */
template <typename K, std::size_t n>
Vector<K, n> forward_substitution(const Matrix<K, n, n>& L,
                                  const Vector<K, n>&    b)
{
	Vector<K, n> x;

	for (decltype(n) m = 0; m < n; ++m) {
		auto sum = K();
		for (decltype(n) i = 0; i < m; ++i) {
			sum = std::fma(L[m][i], x[i], sum);
		}
		x[m] = (b[m] - sum) / L[m][m];
	}

	return x;
}

/*
  U must be an upper triangular matrix.
  let x be the return vale, U*x == b

  time complexity: O(n^2)
  space complexity: O(n)
  */
template <typename K, std::size_t n>
Vector<K, n> back_substitution(const Matrix<K, n, n>& U, const Vector<K, n>& b)
{
	Vector<K, n> x;

	for (ssize_t m = n - 1; m >= 0; --m) {
		auto sum = K();
		for (decltype(n) i = m + 1; i < n; ++i) {
			sum = std::fma(U[m][i], x[i], sum);
		}
		x[m] = (b[m] - sum) / U[m][m];
	}

	return x;
}

/*
  L must be a lower triangular matrix.
  let X be the return vale, L*X == B

  n being the number of element of the matrix
  time complexity: O(n^2)
  space complexity: O(n)
 */
template <typename K, std::size_t n>
Matrix<K, n, n> forward_substitution(const Matrix<K, n, n>& L,
                                     const Matrix<K, n, n>& B)
{
	Matrix<K, n, n> X;

	for (decltype(n) m = 0; m < n; ++m) {
		auto sum = Vector<K, n>();
		for (decltype(n) i = 0; i < m; ++i) {
			sum = std::fma(L[m][i], X[i], sum);
		}
		X[m] = (B[m] - sum) / L[m][m];
	}

	return X;
}

/*
  U must be an upper triangular matrix.
  let X be the return vale, U*X == B

  n being the number of element of the matrix
  time complexity: O(n^2)
  space complexity: O(n)
 */
template <typename K, std::size_t n>
Matrix<K, n, n> back_substitution(const Matrix<K, n, n>& U,
                                  const Matrix<K, n, n>& B)
{
	Matrix<K, n, n> X;

	for (ssize_t m = n - 1; m >= 0; --m) {
		auto sum = Vector<K, n>();
		for (decltype(n) i = m + 1; i < n; ++i) {
			sum = std::fma(U[m][i], X[i], sum);
		}
		X[m] = (B[m] - sum) / U[m][m];
	}

	return X;
}

#endif // SUBSTITUTIONS_H_
