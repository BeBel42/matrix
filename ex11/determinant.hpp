#ifndef DETERMINANT_H_
#define DETERMINANT_H_

#include "../inc/matrix.hpp"
#include <optional>

namespace ns_determinant
{

// find next leftmost pivot
template <typename K, std::size_t n>
static std::optional<std::pair<decltype(n), decltype(n)>>
get_leftmost(const Matrix<K, n, n>& mat, const decltype(n) x,
             const decltype(n) y)
{
	// return next non null coordinates
	for (auto ix = x; ix < n; ++ix) {
		for (auto iy = y; iy < n; ++iy) {
			if (mat[iy][ix] != K()) {
				return std::pair<decltype(n), decltype(n)>{ix, iy};
			}
		}
	}
	// nothing found - empty optional
	return {};
}

// get LU decomposition optimized for determinant
template <typename K, std::size_t n>
static void LU_decomposition(Matrix<K, n, n>& u, const decltype(n) x,
                             const decltype(n) y, bool& is_positive)
{
	// find next pivot
	const auto leftmost = get_leftmost(u, x, y);
	// no next pivot - done
	if (!leftmost) {
		return;
	}
	// pivot coordinates {x, y}
	auto lm = *leftmost;

	// swap pivot with corner
	std::swap(u[y], u[lm.second]);
	// computing number of consecutive swaps needed for this move
	const unsigned diff   = std::max(y, lm.second) - std::min(y, lm.second);
	const unsigned nswaps = diff == 0 ? 0 : std::fma(diff, 2, -1);
	// if nswaps is odd - P's determinant's sign is flipped
	if (nswaps & 1) {
		is_positive = !is_positive;
	}
	lm.second = y;

	/*
	  set all values below to zero using elementary ops
	  the L matrix is ignored - since its determinant is always 1
	  */
	for (decltype(n) i = lm.second + 1; i < n; ++i) {
		const auto multiplier = (u[i][lm.first] / u[lm.second][lm.first]);
		// equivalent to u[i] -= u[lm.second] * multiplier;
		u[i] = std::fma(u[lm.second], -multiplier, u[i]);
	}

	// do the same for the next submatrix
	LU_decomposition(u, lm.first + 1, lm.second + 1, is_positive);
}

} // namespace ns_determinant

/*
  returns determinant using LU factorization with partial pivoting
  source: https://en.wikipedia.org/wiki/LU_decomposition#Algorithms

  time complexity: O(n^3) according to wikipedia < O(n^2) according to my brain
  space complexity: O(3n) == O(n)

  the basic recursive - reuse smaller dimension method has a time complexity
  above O(n^3). so I cannot use it - PDF mistake?
 */
template <typename K, std::size_t n>
K determinant(const Matrix<K, n, n>& a)
{
	// U matrix
	Matrix<K, n, n> u = a;

	/*
	  sign of determinant of permutation matrix P
	  P's determinant is either -1 or 1 according to the number of swaps.
	  thus only the sign needs to be tracked
	   */
	auto is_positive = true;

	// modifies a, u, and is_positive
	ns_determinant::LU_decomposition(u, 0, 0, is_positive);

	// multiply both main diagonals
	K r = 1;
	for (decltype(n) i = 0; i < n; ++i) {
		r *= u[i][i];
	}

	// return det(U) * det(L) * det(P)
	return r * std::fma(int(is_positive), 2, -1);
}

#endif // DETERMINANT_H_
